let nameTabs = document.querySelectorAll('.tabs_link')
let contentText = document.querySelectorAll('.tabs_text')

nameTabs.forEach(function(element){
    element.addEventListener('click', function(){

        let name_title = element.getAttribute('data-title')

        nameTabs.forEach(function(element){
            element.classList.remove('active')
        })
        contentText.forEach(function(item){
            item.setAttribute('hidden', 'true')

            const name_content = item.getAttribute('data-content')

            if(name_title === name_content){
                element.classList.add('active')
                item.removeAttribute('hidden')
            }
        })
    })
})